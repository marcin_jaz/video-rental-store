package com.video.rental.integration;

import com.video.rental.model.entities.Customer;
import com.video.rental.model.entities.Video;
import com.video.rental.model.VideoType;
import com.video.rental.service.CustomerService;
import com.video.rental.service.RentalService;
import com.video.rental.service.VideoService;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractRentalApplicationTest {

    private static final RandomString RANDOM_STRING_GENERATOR = new RandomString();

    @Autowired
    protected CustomerService customerService;
    @Autowired
    protected VideoService videoService;
    @Autowired
    protected RentalService rentalService;

    protected Video addRegularVideo() {
        return videoService.addVideo(Video.VideoBuilder.aVideo()
                .withCopiesAvailable(1)
                .withTitle(randomString())
                .withType(VideoType.REGULAR)
                .build());
    }

    protected Video addNewVideo() {
        return videoService.addVideo(Video.VideoBuilder.aVideo()
                .withCopiesAvailable(1)
                .withTitle(randomString())
                .withType(VideoType.NEW)
                .build());
    }

    protected Customer addCustomer() {
        return customerService.addCustomer(new Customer(randomString()));
    }

    protected String randomString() {
        return RANDOM_STRING_GENERATOR.nextString();
    }
}
