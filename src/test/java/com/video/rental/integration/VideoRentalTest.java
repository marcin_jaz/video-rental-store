package com.video.rental.integration;

import com.video.rental.PriceAssert;
import com.video.rental.model.dto.RentVideosRequest;
import com.video.rental.dao.VideoDao;
import com.video.rental.model.entities.Customer;
import com.video.rental.model.entities.Video;
import com.video.rental.model.dto.Price;
import com.video.rental.model.dto.RentalEndInfo;
import com.video.rental.model.dto.RentalStartInfo;
import org.assertj.core.util.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VideoRentalTest extends AbstractRentalApplicationTest {

    @Autowired
    private VideoDao videoDao;

    @Test
    public void shouldCalculateCorrectPriceAndBonusPointsWhenRentingOneVideo() {
        //assume
        Customer customer = addCustomer();
        Video video = addRegularVideo();
        //given
        RentVideosRequest rentalRequest = RentVideosRequest.RentVideosRequestBuilder.aRentVideosRequest()
                .withCustomerId(customer.getId())
                .withRentalLengthInDays(5)
                .withVideosIds(Sets.newLinkedHashSet(video.getId()))
                .build();
        //when
        RentalStartInfo rentalStartInfo = rentalService.rentVideos(rentalRequest);
        //then
        PriceAssert.assertThat(rentalStartInfo.getPriceToPay()).isEqualTo(Price.amountInSEK("90"));
        assertThat(rentalStartInfo.getBonusPointsEarned()).isEqualTo(1);
    }

    @Test
    public void shouldCalculateCorrectPriceAndBonusPointsWhenRentingTwoVideos() {
        //assume
        Customer customer = addCustomer();
        Video video1 = addRegularVideo();
        Video video2 = addNewVideo();
        //given
        RentVideosRequest rentalRequest = RentVideosRequest.RentVideosRequestBuilder.aRentVideosRequest()
                .withCustomerId(customer.getId())
                .withRentalLengthInDays(5)
                .withVideosIds(Sets.newLinkedHashSet(video1.getId(), video2.getId()))
                .build();
        //when
        RentalStartInfo rentalStartInfo = rentalService.rentVideos(rentalRequest);
        //then
        PriceAssert.assertThat(rentalStartInfo.getPriceToPay()).isEqualTo(Price.amountInSEK("290"));
        assertThat(rentalStartInfo.getBonusPointsEarned()).isEqualTo(3);
    }

    @Test
    public void shouldSumCustomerBonusPointsFromTwoRentals() {
        //assume
        Customer customer = addCustomer();
        Video video1 = addRegularVideo();
        Video video2 = addNewVideo();
        //given
        RentVideosRequest rentalRequest = RentVideosRequest.RentVideosRequestBuilder.aRentVideosRequest()
                .withCustomerId(customer.getId())
                .withRentalLengthInDays(1)
                .build();
        //when
        rentalRequest.setVideosIds(Sets.newLinkedHashSet(video1.getId()));
        rentalService.rentVideos(rentalRequest);
        rentalRequest.setVideosIds(Sets.newLinkedHashSet(video2.getId()));
        rentalService.rentVideos(rentalRequest);
        //then
        Customer updatedCustomer = customerService.getCustomerById(customer.getId());
        assertThat(updatedCustomer.getBonusPoints()).isEqualTo(3);
    }

    @Test
    public void shouldUpdateEndDateAndCalculateSurchargeWhenReturningVideoOnTime() {
        //assume
        Customer customer = addCustomer();
        Video video = addRegularVideo();
        //given
        RentVideosRequest rentalRequest = RentVideosRequest.RentVideosRequestBuilder.aRentVideosRequest()
                .withCustomerId(customer.getId())
                .withRentalLengthInDays(2)
                .withVideosIds(Sets.newLinkedHashSet(video.getId()))
                .build();
        RentalStartInfo rentalStartInfo = rentalService.rentVideos(rentalRequest);
        //when
        RentalEndInfo rentalEndInfo = rentalService.returnVideos(rentalStartInfo.getRentalId());
        //then
        assertThat(rentalEndInfo.getRental().getEndDate()).isNotNull();
        PriceAssert.assertThat(rentalEndInfo.getSurchargeToPay()).isEqualTo(Price.ZERO);
    }

    @Test
    public void shouldMaintainNumberOfCopiesWhenRentingAndReturningTheVideo() {
        //assume
        Customer customer = addCustomer();
        Video video = addRegularVideo();
        //given
        RentVideosRequest rentalRequest = RentVideosRequest.RentVideosRequestBuilder.aRentVideosRequest()
                .withCustomerId(customer.getId())
                .withRentalLengthInDays(2)
                .withVideosIds(Sets.newLinkedHashSet(video.getId()))
                .build();
        //when
        RentalStartInfo rentalStartInfo = rentalService.rentVideos(rentalRequest);
        Video videoWhenRented = videoDao.findById(video.getId()).get();
        rentalService.returnVideos(rentalStartInfo.getRentalId());
        Video videoWhenReturned = videoDao.findById(video.getId()).get();
        //then
        assertThat(videoWhenRented.getCopiesAvailable()).isZero();
        assertThat(videoWhenReturned.getCopiesAvailable()).isOne();
    }

}
