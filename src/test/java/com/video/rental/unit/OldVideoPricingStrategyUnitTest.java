package com.video.rental.unit;

import com.video.rental.model.dto.Price;
import com.video.rental.service.ConfigurationService;
import com.video.rental.service.pricing.OldVideoPricingStrategy;
import com.video.rental.service.pricing.PricingStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.video.rental.PriceAssert.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OldVideoPricingStrategyUnitTest {

    @Mock
    private ConfigurationService configurationService;
    @InjectMocks
    private PricingStrategy strategy = new OldVideoPricingStrategy();

    @Test
    public void shouldThrowExceptionWhenTryingToRentForZeroDays() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> strategy.calculate(0));
    }

    @Test
    public void shouldReturnBasicPriceWhenRentedForOneDay() {
        //given
        Price price = Price.amountInSEK("30");
        when(configurationService.getBasicPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(1);
        //then
        assertThat(result).isEqualTo(price);
    }

    @Test
    public void shouldReturnBasicPriceWhenRentedForFiveDays() {
        //given
        Price price = Price.amountInSEK("90");
        when(configurationService.getBasicPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(5);
        //then
        assertThat(result).isEqualTo(price);
    }

    @Test
    public void shouldReturnBasicPriceForFiveDaysAndBasicPriceForTheSixthDay() {
        //given
        Price price = Price.amountInSEK("200.82");
        when(configurationService.getBasicPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(6);
        //then
        assertThat(result).isEqualTo(Price.amountInSEK("401.64"));
    }

    @Test
    public void shouldReturnBasicPriceForFiveDaysAndBasicPriceTimesTheDaysAbove() {
        //given
        Price price = Price.amountInSEK("30");
        when(configurationService.getBasicPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(7);
        //then
        assertThat(result).isEqualTo(Price.amountInSEK("90"));
    }

}
