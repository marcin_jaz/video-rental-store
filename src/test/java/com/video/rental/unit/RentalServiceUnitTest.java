package com.video.rental.unit;

import com.video.rental.dao.RentalDao;
import com.video.rental.model.entities.Rental;
import com.video.rental.service.RentalService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RentalServiceUnitTest {

    @Mock
    private RentalDao rentalDao;
    @InjectMocks
    private RentalService rentalService = new RentalService();

    @Test
    public void shouldThrowAnExceptionOnRentalReturnIfItWasAlreadyEndedBefore() {
        //given
        Rental rental = Rental.RentalBuilder.aRental()
                .withId(123L)
                .withEndDate(LocalDate.now())
                .build();
        when(rentalDao.getOne(rental.getId())).thenReturn(rental);
        //then
        assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(() -> rentalService.returnVideos(rental.getId()));
    }

    //TODO normally rent and return methods should be covered further

}
