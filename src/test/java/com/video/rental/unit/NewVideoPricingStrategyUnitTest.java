package com.video.rental.unit;

import com.video.rental.PriceAssert;
import com.video.rental.model.dto.Price;
import com.video.rental.service.ConfigurationService;
import com.video.rental.service.pricing.NewVideoPricingStrategy;
import com.video.rental.service.pricing.PricingStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NewVideoPricingStrategyUnitTest {

    @Mock
    private ConfigurationService configurationService;
    @InjectMocks
    private PricingStrategy strategy = new NewVideoPricingStrategy();

    @Test
    public void shouldThrowExceptionWhenTryingToRentForZeroDays() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> strategy.calculate(0));
    }

    @Test
    public void shouldReturnPremiumPriceWhenRentedForOneDay() {
        //given
        Price price = Price.amountInSEK("100");
        when(configurationService.getPremiumPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(1);
        //then
        PriceAssert.assertThat(result).isEqualTo(price);
    }

    @Test
    public void shouldReturnPremiumPriceForEachDayOfRentalWhenRentedForThreeDays() {
        //given
        Price price = Price.amountInSEK("21.21");
        when(configurationService.getPremiumPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(3);
        //then
        PriceAssert.assertThat(result).isEqualTo(Price.amountInSEK("63.63"));
    }

    @Test
    public void shouldReturnPremiumPriceForEachDayOfRentalWhenRentedForTenDays() {
        //given
        Price price = Price.amountInSEK("89.87");
        when(configurationService.getPremiumPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(10);
        //then
        PriceAssert.assertThat(result).isEqualTo(Price.amountInSEK("898.70"));
    }
}
