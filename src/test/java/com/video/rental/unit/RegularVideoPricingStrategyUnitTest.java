package com.video.rental.unit;

import com.video.rental.model.dto.Price;
import com.video.rental.service.ConfigurationService;
import com.video.rental.service.pricing.PricingStrategy;
import com.video.rental.service.pricing.RegularVideoPricingStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.video.rental.PriceAssert.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RegularVideoPricingStrategyUnitTest {

    @Mock
    private ConfigurationService configurationService;
    @InjectMocks
    private PricingStrategy strategy = new RegularVideoPricingStrategy();

    @Test
    public void shouldThrowExceptionWhenTryingToRentForZeroDays() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> strategy.calculate(0));
    }

    @Test
    public void shouldReturnBasicPriceWhenRentedForOneDay() {
        //given
        Price price = Price.amountInSEK("40");
        when(configurationService.getBasicPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(1);
        //then
        assertThat(result).isEqualTo(price);
    }

    @Test
    public void shouldReturnBasicPriceWhenRentedForThreeDays() {
        //given
        Price price = Price.amountInSEK("12.99");
        when(configurationService.getBasicPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(3);
        //then
        assertThat(result).isEqualTo(price);
    }

    @Test
    public void shouldReturnBasicPriceForThreeDaysAndBasicPriceForTheFourthDay() {
        //given
        Price price = Price.amountInSEK("100.99");
        when(configurationService.getBasicPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(4);
        //then
        assertThat(result).isEqualTo(Price.amountInSEK("201.98"));
    }

    @Test
    public void shouldReturnBasicPriceForThreeDaysAndBasicPriceTimesTheDaysAbove() {
        //given
        Price price = Price.amountInSEK("30");
        when(configurationService.getBasicPrice()).thenReturn(price);
        //when
        Price result = strategy.calculate(5);
        //then
        assertThat(result).isEqualTo(Price.amountInSEK("90"));
    }
}
