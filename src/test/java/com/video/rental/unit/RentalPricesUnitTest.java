package com.video.rental.unit;

import com.video.rental.model.dto.Price;
import com.video.rental.model.entities.Rental;
import com.video.rental.model.entities.Video;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.time.LocalDate;

import static com.video.rental.PriceAssert.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

public class RentalPricesUnitTest {

    @Test
    public void shouldCalculateThePriceToPayForTwoVideos() {
        //assume
        final int daysRented = 2;
        //given
        Video video1 = mock(Video.class);
        when(video1.calculateRentPrice(daysRented)).thenReturn(Price.amountInSEK("20"));
        Video video2 = mock(Video.class);
        when(video2.calculateRentPrice(daysRented)).thenReturn(Price.amountInSEK("30"));

        Rental rental = Rental.RentalBuilder.aRental()
                .withVideos(Lists.newArrayList(video1, video2))
                .build();
        //when
        Price price = rental.getPriceToPay(daysRented);
        //then
        assertThat(price).isEqualTo(Price.amountInSEK("50"));
    }

    @Test
    public void shouldThrowExceptionIfTryingToCalculateSurchargeWithoutEndDateGiven() {
        Rental rental = new Rental();
        assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(() -> rental.getSurchargeToPayOnReturn());
    }

    @Test
    public void shouldReturnZeroAsTheSurchargeIfVideosReturnedEarlier() {
        //given
        LocalDate expectedEndDate = LocalDate.now();
        Rental rental = Rental.RentalBuilder.aRental()
                .withStartDate(expectedEndDate.minusDays(5))
                .withExpectedEndDate(expectedEndDate)
                .withEndDate(expectedEndDate.minusDays(2))
                .build();
        //when
        Price price = rental.getSurchargeToPayOnReturn();
        //then
        assertThat(price).isEqualTo(Price.ZERO);
    }

    @Test
    public void shouldReturnZeroAsTheSurchargeIfVideosReturnedExactlyOnTime() {
        //given
        LocalDate expectedEndDate = LocalDate.now();
        Rental rental = Rental.RentalBuilder.aRental()
                .withStartDate(expectedEndDate.minusDays(5))
                .withExpectedEndDate(expectedEndDate)
                .withEndDate(expectedEndDate)
                .build();
        //when
        Price price = rental.getSurchargeToPayOnReturn();
        //then
        assertThat(price).isEqualTo(Price.ZERO);
    }

    @Test
    public void shouldReturnPositiveSurchargeIfVideoReturnedAfterExpectedDate() {
        //given
        LocalDate expectedEndDate = LocalDate.now();
        Rental rental = spy(Rental.RentalBuilder.aRental()
                .withStartDate(expectedEndDate.minusDays(5))
                .withExpectedEndDate(expectedEndDate)
                .withEndDate(expectedEndDate.plusDays(1))
                .build());
        doReturn(Price.amountInSEK("30")).when(rental).getPriceToPay(5);
        doReturn(Price.amountInSEK("50")).when(rental).getPriceToPay(6);
        //when
        Price price = rental.getSurchargeToPayOnReturn();
        //then
        assertThat(price).isEqualTo(Price.amountInSEK("20"));
    }
}
