package com.video.rental;

import com.video.rental.model.dto.Price;
import org.assertj.core.api.AbstractAssert;

/**
 * Custom assertion for testing the price
 */
public class PriceAssert extends AbstractAssert<PriceAssert, Price> {

    public PriceAssert(Price actual) {
        super(actual, PriceAssert.class);
    }

    public static PriceAssert assertThat(Price actual) {
        return new PriceAssert(actual);
    }

    @Override
    public PriceAssert isEqualTo(Object expected) {
        isNotNull();
        if (!(expected instanceof Price)) {
            failWithMessage("Expected an object of the class <%s> but was <%s>",
                    Price.class.getSimpleName(), expected.getClass().getSimpleName());
        }
        Price expectedPrice = (Price) expected;
        if (expectedPrice.compareTo(actual) != 0) {
            failWithMessage("Expected amount to be <%s> but was <%s>", expectedPrice.getAmount(), actual.getAmount());
        }
        return this;
    }
}
