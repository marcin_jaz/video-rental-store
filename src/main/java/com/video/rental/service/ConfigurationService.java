package com.video.rental.service;

import com.video.rental.model.dto.Price;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationService {

    private static final Price BASIC_PRICE = Price.amountInSEK("30");
    private static final Price PREMIUM_PRICE = Price.amountInSEK("40");

    public Price getBasicPrice() {
        return BASIC_PRICE;
    }

    public Price getPremiumPrice() {
        return PREMIUM_PRICE;
    }

}
