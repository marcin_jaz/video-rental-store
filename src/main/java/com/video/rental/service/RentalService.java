package com.video.rental.service;

import com.video.rental.model.dto.RentVideosRequest;
import com.video.rental.dao.RentalDao;
import com.video.rental.model.entities.Customer;
import com.video.rental.model.entities.Rental;
import com.video.rental.model.entities.Video;
import com.video.rental.model.dto.Price;
import com.video.rental.model.dto.RentalEndInfo;
import com.video.rental.model.dto.RentalStartInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RentalService {

    @Autowired
    private RentalDao rentalDao;
    @Autowired
    private VideoService videoService;
    @Autowired
    private CustomerService customerService;

    public RentalStartInfo rentVideos(RentVideosRequest request) {
        Customer customer = customerService.getCustomerById(request.getCustomerId());

        List<Video> videos = videoService.getVideosCheckingIfAvailable(request.getVideosIds());
        videoService.decrementNumberOfCopiesAvailable(videos);

        Rental rental = addRental(customer, videos, request.getRentalLengthInDays());

        int bonusPoints = rental.getBonusPoints();
        customerService.addBonusPoints(customer, bonusPoints);

        return RentalStartInfo.RentalStartInfoBuilder.aRentalStartInfo()
                .withRental(rental)
                .withBonusPointsEarned(bonusPoints)
                .withPriceToPay(rental.getPriceToPay(request.getRentalLengthInDays()))
                .build();
    }

    private Rental addRental(Customer customer, List<Video> videos, int numberOfDays) {
        LocalDate today = LocalDate.now();
        Rental rental = Rental.RentalBuilder.aRental()
                .withCustomer(customer)
                .withVideos(videos)
                .withStartDate(today)
                .withExpectedEndDate(today.plusDays(numberOfDays))
                .build();
        return rentalDao.save(rental);
    }

    public RentalEndInfo returnVideos(Long rentalId) {
        Rental rental = attemptToFinishRental(rentalId);

        videoService.incrementNumberOfCopiesAvailable(rental.getVideos());

        Price surcharge = rental.getSurchargeToPayOnReturn();
        return new RentalEndInfo(rental, surcharge);
    }

    private Rental attemptToFinishRental(Long rentalId) {
        Rental rental = rentalDao.getOne(rentalId);
        if (rental.isFinished()) {
            throw new IllegalStateException("Rental is already ended " + rentalId);
        }
        rental.setEndDate(LocalDate.now());
        return rentalDao.save(rental);
    }

}
