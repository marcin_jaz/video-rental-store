package com.video.rental.service;

import com.video.rental.dao.VideoDao;
import com.video.rental.model.entities.Video;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class VideoService {

    @Autowired
    private VideoDao videoDao;

    public List<Video> getAllVideos() {
        return videoDao.findAll();
    }

    public Video addVideo(Video video) {
        return videoDao.save(video);
    }

    public List<Video> getVideosCheckingIfAvailable(Set<Long> videosIds) {
        List<Video> videos = videoDao.findAllById(videosIds);
        if (videos.size() != videosIds.size()) {
            //TODO add wrong ids to the error message
            throw new IllegalArgumentException("Not all videos with given ids available in the store.");
        }
        if (videos.stream().anyMatch(Video::isUnavailable)) {
            //TODO add unavailable videos to the error message
            throw new IllegalArgumentException("Not all videos with given ids have copies available for rental.");
        }
        return videos;
    }

    public void decrementNumberOfCopiesAvailable(List<Video> videos) {
        videos.forEach(Video::decrementNumberOfCopies);
        videoDao.saveAll(videos);
    }

    public void incrementNumberOfCopiesAvailable(List<Video> videos) {
        videos.forEach(Video::incrementNumberOfCopies);
        videoDao.saveAll(videos);
    }
}
