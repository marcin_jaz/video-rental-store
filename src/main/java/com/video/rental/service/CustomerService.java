package com.video.rental.service;

import com.video.rental.dao.CustomerDao;
import com.video.rental.model.entities.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@Service
@Transactional
public class CustomerService {

    @Autowired
    private CustomerDao customerDao;

    public Customer getCustomerById(Long id) {
        return customerDao.findById(id).orElseThrow(() -> new EntityNotFoundException());
    }

    public Customer addCustomer(Customer video) {
        return customerDao.save(video);
    }

    public void addBonusPoints(Customer customer, int bonusPoints) {
        customer.addBonusPoints(bonusPoints);
        customerDao.save(customer);
    }
}
