package com.video.rental.service.pricing;

import com.video.rental.model.VideoType;
import com.video.rental.model.dto.Price;
import org.springframework.stereotype.Service;

@Service
public class RegularVideoPricingStrategy extends AbstractPricingStrategy {

    public RegularVideoPricingStrategy() {
        super(VideoType.REGULAR);
    }

    @Override
    protected Price getPricePerDay() {
        return getBasicPrice();
    }

    @Override
    protected int getDaysNumberCoveredWithFirstPayment() {
        return 3;
    }
}
