package com.video.rental.service.pricing;

import com.video.rental.model.VideoType;
import com.video.rental.model.dto.Price;
import org.springframework.stereotype.Service;

@Service
public class NewVideoPricingStrategy extends AbstractPricingStrategy {

    public NewVideoPricingStrategy() {
        super(VideoType.NEW);
    }

    @Override
    protected Price getPricePerDay() {
        return getPremiumPrice();
    }

    @Override
    protected int getDaysNumberCoveredWithFirstPayment() {
        return 1;
    }
}
