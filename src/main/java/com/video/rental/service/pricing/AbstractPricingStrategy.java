package com.video.rental.service.pricing;

import com.video.rental.model.VideoType;
import com.video.rental.model.dto.Price;
import com.video.rental.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractPricingStrategy implements PricingStrategy {

    @Autowired
    private ConfigurationService configurationService;

    protected AbstractPricingStrategy(VideoType videoType) {
        videoType.setPricingStrategy(this);
    }

    @Override
    public Price calculate(int daysRented) {
        if (daysRented < 1) {
            throw new IllegalArgumentException("It's only possible to rent a video for one day or more.");
        }
        if (daysRented <= getDaysNumberCoveredWithFirstPayment()) {
            return getPricePerDay();
        }
        int regularPriceDays = daysRented - (getDaysNumberCoveredWithFirstPayment() - 1);
        return getPricePerDay().multiply(regularPriceDays);
    }

    /**
     * @return price per day when renting an item
     */
    protected abstract Price getPricePerDay();

    /**
     * @return number of days that is covered with the first payment, after this number of days passes the price per
     * each next day is added to the amount to be paid.
     */
    protected abstract int getDaysNumberCoveredWithFirstPayment();

    protected Price getBasicPrice() {
        return configurationService.getBasicPrice();
    }

    protected Price getPremiumPrice() {
        return configurationService.getPremiumPrice();
    }
}
