package com.video.rental.service.pricing;

import com.video.rental.model.dto.Price;

public interface PricingStrategy {

    /**
     * Calculate the price to be paid given the number of days that the item is rented for.
     *
     * @param daysRented number of days that the item is rented for
     * @return Price to be paid by the customer
     */
    Price calculate(int daysRented);
}
