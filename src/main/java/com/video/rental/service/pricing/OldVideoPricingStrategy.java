package com.video.rental.service.pricing;

import com.video.rental.model.VideoType;
import com.video.rental.model.dto.Price;
import org.springframework.stereotype.Service;

@Service
public class OldVideoPricingStrategy extends AbstractPricingStrategy {

    public OldVideoPricingStrategy() {
        super(VideoType.OLD);
    }

    @Override
    protected Price getPricePerDay() {
        return getBasicPrice();
    }

    @Override
    protected int getDaysNumberCoveredWithFirstPayment() {
        return 5;
    }
}
