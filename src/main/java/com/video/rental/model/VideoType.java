package com.video.rental.model;

import com.video.rental.model.dto.Price;
import com.video.rental.service.pricing.PricingStrategy;

public enum VideoType {
    NEW(2),
    REGULAR(1),
    OLD(1);

    private final int bonusPointsPerRental;
    private PricingStrategy pricingStrategy;

    VideoType(int bonusPointsPerRental) {
        this.bonusPointsPerRental = bonusPointsPerRental;
    }

    public int getBonusPointsPerRental() {
        return bonusPointsPerRental;
    }

    public Price calculateRentPrice(int daysRented) {
        return pricingStrategy.calculate(daysRented);
    }

    public void setPricingStrategy(PricingStrategy pricingStrategy) {
        this.pricingStrategy = pricingStrategy;
    }
}
