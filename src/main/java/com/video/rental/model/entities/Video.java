package com.video.rental.model.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.video.rental.model.VideoType;
import com.video.rental.model.dto.Price;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "videos")
public class Video implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Version
    private long version;
    @NotBlank
    @Column
    private String title;
    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private VideoType type;
    @Column
    private int copiesAvailable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public VideoType getType() {
        return type;
    }

    public Price calculateRentPrice(int daysRented) {
        return getType().calculateRentPrice(daysRented);
    }

    @JsonIgnore
    public int getRentBonusPoints() {
        return getType().getBonusPointsPerRental();
    }

    public void setType(VideoType type) {
        this.type = type;
    }

    @JsonIgnore
    public boolean isUnavailable() {
        return copiesAvailable <= 0;
    }

    public int getCopiesAvailable() {
        return copiesAvailable;
    }

    public void setCopiesAvailable(int copiesAvailable) {
        this.copiesAvailable = copiesAvailable;
    }

    public void decrementNumberOfCopies() {
        copiesAvailable--;
    }

    public void incrementNumberOfCopies() {
        copiesAvailable++;
    }

    public static final class VideoBuilder {
        private String title;
        private VideoType type;
        private int copiesAvailable;

        private VideoBuilder() {
        }

        public static VideoBuilder aVideo() {
            return new VideoBuilder();
        }

        public VideoBuilder withTitle(String title) {
            this.title = title;
            return this;
        }

        public VideoBuilder withType(VideoType type) {
            this.type = type;
            return this;
        }

        public VideoBuilder withCopiesAvailable(int copiesAvailable) {
            this.copiesAvailable = copiesAvailable;
            return this;
        }

        public Video build() {
            Video video = new Video();
            video.setTitle(title);
            video.setType(type);
            video.setCopiesAvailable(copiesAvailable);
            return video;
        }
    }
}
