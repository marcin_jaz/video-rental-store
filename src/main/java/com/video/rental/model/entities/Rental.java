package com.video.rental.model.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.video.rental.model.dto.Price;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "rentals")
public class Rental implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Version
    private long version;
    @Column
    private LocalDate startDate;
    @Column
    private LocalDate expectedEndDate;
    @Column
    private LocalDate endDate;
    @NotEmpty
    @ManyToMany
    private List<Video> videos = new ArrayList<>();
    @OneToOne
    private Customer customer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(LocalDate expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @JsonIgnore
    public int getBonusPoints() {
        return videos.stream()
                .mapToInt(Video::getRentBonusPoints)
                .sum();
    }

    public Price getPriceToPay(int daysRented) {
        return videos.stream()
                .map(video -> video.calculateRentPrice(daysRented))
                .reduce(Price::add)
                .orElseThrow(() -> new IllegalStateException("Rental must have at least one video."));
    }

    @JsonIgnore
    public Price getSurchargeToPayOnReturn() {
        if (getEndDate() == null) {
            throw new IllegalStateException("End date must be given to calculate the surcharge");
        }
        int daysAlreadyPaid = Period.between(getStartDate(), getExpectedEndDate()).getDays();
        int totalDays = Period.between(getStartDate(), getEndDate()).getDays();
        if (daysAlreadyPaid < totalDays) {
            Price pricePaidAtRent = getPriceToPay(daysAlreadyPaid);
            Price totalPrice = getPriceToPay(totalDays);
            return totalPrice.subtract(pricePaidAtRent);
        }
        return Price.ZERO;
    }

    public boolean isFinished() {
        return getEndDate() != null;
    }

    public static final class RentalBuilder {
        private Long id;
        private LocalDate startDate;
        private LocalDate expectedEndDate;
        private LocalDate endDate;
        private List<Video> videos = new ArrayList<>();
        private Customer customer;

        private RentalBuilder() {
        }

        public static RentalBuilder aRental() {
            return new RentalBuilder();
        }

        public RentalBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public RentalBuilder withStartDate(LocalDate startDate) {
            this.startDate = startDate;
            return this;
        }

        public RentalBuilder withExpectedEndDate(LocalDate expectedEndDate) {
            this.expectedEndDate = expectedEndDate;
            return this;
        }

        public RentalBuilder withEndDate(LocalDate endDate) {
            this.endDate = endDate;
            return this;
        }

        public RentalBuilder withVideos(List<Video> videos) {
            this.videos = videos;
            return this;
        }

        public RentalBuilder withCustomer(Customer customer) {
            this.customer = customer;
            return this;
        }

        public Rental build() {
            Rental rental = new Rental();
            rental.setId(id);
            rental.setStartDate(startDate);
            rental.setExpectedEndDate(expectedEndDate);
            rental.setEndDate(endDate);
            rental.setVideos(videos);
            rental.setCustomer(customer);
            return rental;
        }
    }
}
