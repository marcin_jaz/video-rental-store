package com.video.rental.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.video.rental.model.entities.Rental;

import java.io.Serializable;

/**
 * Information for the customer that get's returned on the start of the rental
 */
public class RentalStartInfo implements Serializable {

    private Rental rental;
    private Price priceToPay;
    private int bonusPointsEarned;

    /**
     * @return details of the rental, containing eg. the information about the videos being rented and expected return date
     */
    public Rental getRental() {
        return rental;
    }

    /**
     * @return price that the customer needs to pay at the time of starting the rental
     */
    public Price getPriceToPay() {
        return priceToPay;
    }

    /**
     * @return number of bonus points that the customer earned with the current rental.
     */
    public int getBonusPointsEarned() {
        return bonusPointsEarned;
    }

    @JsonIgnore
    public Long getRentalId() {
        return rental.getId();
    }

    public static final class RentalStartInfoBuilder {
        private Rental rental;
        private Price priceToPay;
        private int bonusPointsEarned;

        private RentalStartInfoBuilder() {
        }

        public static RentalStartInfoBuilder aRentalStartInfo() {
            return new RentalStartInfoBuilder();
        }

        public RentalStartInfoBuilder withRental(Rental rental) {
            this.rental = rental;
            return this;
        }

        public RentalStartInfoBuilder withPriceToPay(Price priceToPay) {
            this.priceToPay = priceToPay;
            return this;
        }

        public RentalStartInfoBuilder withBonusPointsEarned(int bonusPointsEarned) {
            this.bonusPointsEarned = bonusPointsEarned;
            return this;
        }

        public RentalStartInfo build() {
            RentalStartInfo rentalStartInfo = new RentalStartInfo();
            rentalStartInfo.bonusPointsEarned = this.bonusPointsEarned;
            rentalStartInfo.rental = this.rental;
            rentalStartInfo.priceToPay = this.priceToPay;
            return rentalStartInfo;
        }
    }
}
