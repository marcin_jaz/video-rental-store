package com.video.rental.model.dto;

import com.video.rental.model.entities.Rental;

import java.io.Serializable;

/**
 * Information confirming the return of the rental with potential surcharge to pay.
 */
public class RentalEndInfo implements Serializable {

    private final Rental rental;
    private final Price surchargeToPay;

    public RentalEndInfo(Rental rental, Price surchargeToPay) {
        this.rental = rental;
        this.surchargeToPay = surchargeToPay;
    }

    public Rental getRental() {
        return rental;
    }

    public Price getSurchargeToPay() {
        return surchargeToPay;
    }
}
