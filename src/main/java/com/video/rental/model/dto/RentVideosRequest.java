package com.video.rental.model.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Set;

public class RentVideosRequest {

    @NotEmpty
    private Set<Long> videosIds;
    @NotNull
    private long customerId;
    @Positive
    private int rentalLengthInDays;

    public Set<Long> getVideosIds() {
        return videosIds;
    }

    public void setVideosIds(Set<Long> videosIds) {
        this.videosIds = videosIds;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public int getRentalLengthInDays() {
        return rentalLengthInDays;
    }

    public void setRentalLengthInDays(int rentalLengthInDays) {
        this.rentalLengthInDays = rentalLengthInDays;
    }

    public static final class RentVideosRequestBuilder {
        private Set<Long> videosIds;
        private long customerId;
        private int rentalLengthInDays;

        private RentVideosRequestBuilder() {
        }

        public static RentVideosRequestBuilder aRentVideosRequest() {
            return new RentVideosRequestBuilder();
        }

        public RentVideosRequestBuilder withVideosIds(Set<Long> videosIds) {
            this.videosIds = videosIds;
            return this;
        }

        public RentVideosRequestBuilder withCustomerId(long customerId) {
            this.customerId = customerId;
            return this;
        }

        public RentVideosRequestBuilder withRentalLengthInDays(int rentalLengthInDays) {
            this.rentalLengthInDays = rentalLengthInDays;
            return this;
        }

        public RentVideosRequest build() {
            RentVideosRequest rentVideosRequest = new RentVideosRequest();
            rentVideosRequest.setVideosIds(videosIds);
            rentVideosRequest.setCustomerId(customerId);
            rentVideosRequest.setRentalLengthInDays(rentalLengthInDays);
            return rentVideosRequest;
        }
    }
}
