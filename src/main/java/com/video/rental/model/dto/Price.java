package com.video.rental.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

/**
 * Immutable class representing the price
 */
public class Price implements Comparable<Price>, Serializable {

    public static final Price ZERO = new Price(BigDecimal.ZERO);

    private final BigDecimal amount;
    private final Currency currency;

    public static Price amountInSEK(String amount) {
        return new Price(new BigDecimal(amount));
    }

    private Price(BigDecimal amount) {
        this(amount, Currency.getInstance("SEK"));
    }

    private Price(BigDecimal amount, Currency currency) {
        this.currency = currency;
        this.amount = amount.setScale(currency.getDefaultFractionDigits(), RoundingMode.HALF_EVEN);
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    /**
     * Multiply the price by given number of times. It doesn't change the Price object.
     *
     * @param times number of times to multiply by
     * @return the result of this * times
     */
    public Price multiply(int times) {
        BigDecimal resultAmount = getAmount().multiply(new BigDecimal(times));
        return new Price(resultAmount, getCurrency());
    }

    /**
     * Add two prices. It doesn't change the Price object.
     *
     * @param other price
     * @return the result of this + other
     */
    public Price add(Price other) {
        throwExceptionIfCurrenciesDifferent(other);
        BigDecimal resultAmount = getAmount().add(other.getAmount());
        return new Price(resultAmount, getCurrency());
    }

    /**
     * Subtract the given price from the current one. It doesn't change the Price object.
     *
     * @param other price to be subtracted from this
     * @return the result of this - other
     */
    public Price subtract(Price other) {
        throwExceptionIfCurrenciesDifferent(other);
        BigDecimal resultAmount = getAmount().subtract(other.getAmount());
        return new Price(resultAmount, getCurrency());
    }

    private void throwExceptionIfCurrenciesDifferent(Price other) {
        if (!currency.equals(other.getCurrency())) {
            throw new UnsupportedOperationException("It's not possible to perform operations on the prices expressed in different currencies");
        }
    }

    @Override
    public int compareTo(Price other) {
        throwExceptionIfCurrenciesDifferent(other);
        return amount.compareTo(other.getAmount());
    }
}
