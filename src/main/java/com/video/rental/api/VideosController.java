package com.video.rental.api;

import com.video.rental.model.dto.RentVideosRequest;
import com.video.rental.model.dto.RentalEndInfo;
import com.video.rental.model.dto.RentalStartInfo;
import com.video.rental.model.entities.Video;
import com.video.rental.service.CustomerService;
import com.video.rental.service.RentalService;
import com.video.rental.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/videos")
public class VideosController {

    @Autowired
    private VideoService videoService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private RentalService rentalService;

    /**
     * Add video to the inventory
     *
     * @param video to be added
     * @return video added
     */
    @PutMapping
    public Video addVideo(@Valid @RequestBody Video video) {
        return videoService.addVideo(video);
    }

    /**
     * Get all videos available in the inventory.
     *
     * @return list of videos
     */
    @GetMapping
    public List<Video> getAllVideos() {
        return videoService.getAllVideos();
    }

    /**
     * Rent the videos that are already present in the store. it accepts the customerId, ids of the videos that he wants
     * to rent, and the number of days the rent is for. In the response it returns the price to be paid.
     *
     * @param request RentVideosRequest
     * @return RentalStartInfo information about the rental and the price that needs to be paid.
     */
    @PostMapping(path = "/rent")
    public RentalStartInfo rentVideos(@Valid @RequestBody RentVideosRequest request) {
        return rentalService.rentVideos(request);
    }

    /**
     * Return all videos within one rental. It's not possible to return only subset of these
     * videos. Videos of the rental can be returned only once.
     * On successful return the response will contain the surcharge to be paid in case
     * the videos were returned after the expected date.
     *
     * @param rentalId id of the rental to be returned
     * @return confirmation of the return including the potential surcharge to be paid
     */
    @PostMapping(path = "/return")
    public RentalEndInfo returnVideos(@RequestParam @NotNull long rentalId) {
        return rentalService.returnVideos(rentalId);
    }

}
